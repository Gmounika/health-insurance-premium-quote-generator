package com.health.insurance.premium.quote.generator.model;

public class Input {
	private String name;
	private String gender;
	private int age;
	private CurrentHealth currentHealth;
	private Habits habits;
	
	public Input(String name, String gender, int age, CurrentHealth currentHealth, Habits habits) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.currentHealth = currentHealth;
		this.habits = habits;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((currentHealth == null) ? 0 : currentHealth.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((habits == null) ? 0 : habits.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Input other = (Input) obj;
		if (age != other.age)
			return false;
		if (currentHealth == null) {
			if (other.currentHealth != null)
				return false;
		} else if (!currentHealth.equals(other.currentHealth))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (habits == null) {
			if (other.habits != null)
				return false;
		} else if (!habits.equals(other.habits))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Input [name=" + name + ", gender=" + gender + ", age=" + age + ", currentHealth=" + currentHealth
				+ ", habits=" + habits + "]";
	}
	
	
}
