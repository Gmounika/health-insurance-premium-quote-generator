package com.health.insurance.premium.quote.generator;

import static org.junit.Assert.*;

import org.junit.Test;

import com.health.insurance.premium.quote.generator.model.CurrentHealth;
import com.health.insurance.premium.quote.generator.model.Habits;
import com.health.insurance.premium.quote.generator.model.Input;

public class QuoteGeneratorTest {
	private QuoteGenerator quoteGenerator = new QuoteGeneratorImpl();
	@Test
	public void testGetQuote(){
		Habits habit = new Habits("No","Yes","Yes","No");
		CurrentHealth currentHealth = new CurrentHealth("No","No","No","Yes");
		Input input = new Input("Gomes","Yes",33,currentHealth,habit);
		assertEquals(quoteGenerator.getQuote(input),"Health Insurance Premium for Gomes 7387");
	}
	
	@Test
	public void testGetQuoteNegative(){
		Habits habit = new Habits("No","Yes","Yes","No");
		CurrentHealth currentHealth = new CurrentHealth("No","No","No","Yes");
		Input input = new Input("Gomes","Yes",48,currentHealth,habit);
		assertNotEquals(quoteGenerator.getQuote(input),"Health Insurance Premium for Gomes 7387");
	}
}
