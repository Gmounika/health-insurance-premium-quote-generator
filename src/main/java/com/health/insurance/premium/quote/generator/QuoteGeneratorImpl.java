package com.health.insurance.premium.quote.generator;

import com.health.insurance.premium.quote.generator.model.Input;
import com.health.insurance.premium.quote.generator.rules.Rules;

public class QuoteGeneratorImpl implements QuoteGenerator{

	public String getQuote(Input in) {		
		String str = (in.getGender().equals("M"))?"Health Insurance Premium for Mr.":"Health Insurance Premium for ";
		return str+in.getName()+" "+Rules.getInsuranceQuote(5000,in.getGender(),in.getAge(), in.getCurrentHealth(), in.getHabits());
	}

}
