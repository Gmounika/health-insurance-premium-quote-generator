package com.health.insurance.premium.quote.generator;

import com.health.insurance.premium.quote.generator.model.Input;

public interface QuoteGenerator {
	public String getQuote(Input in);
}
